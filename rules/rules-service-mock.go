package rules

import (
	"context"
	"fmt"

	rules "infrontfinance.dev/regfeed/grpc/rules/v2"
)

type RulesServiceMock struct {
	rules.RulesServiceServer
}

func (r *RulesServiceMock) ListTccRules(ctx context.Context, request *rules.ListTccRulesRequest) (*rules.ListTccRulesResponse, error) {
	if request == nil {
		return nil, nil
	}
	if request.GetProductCategoryId()[0] == "3" {
		return nil, fmt.Errorf("Id cannot be 3 :)")
	} else if request.GetProductCategoryId()[0] != "10" {
		// special case in the mock for dealing with a nil response and a nil error
		return nil, nil
	}
	return &rules.ListTccRulesResponse{
		XData: []*rules.GetTccRuleResponse{
			&rules.GetTccRuleResponse{
				TccRule: &rules.TccRule{},
			},
		},
	}, nil
}
