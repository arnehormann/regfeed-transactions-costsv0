package rules

import (
	"context"
	"errors"
	"net"
	"testing"
	"time"

	"golang.org/x/oauth2"
	"google.golang.org/grpc"
	rules "infrontfinance.dev/regfeed/grpc/rules/v2"
	assert "infrontfinance.dev/regfeed/transactions-costsv0/assert"
	grpcclient "infrontfinance.dev/regfeed/transactions-costsv0/grpc"
)

type TokenGeneratorMock struct {
}

func (t *TokenGeneratorMock) Token(ctx context.Context) (*oauth2.Token, error) {
	return &oauth2.Token{}, nil
}

type ErrorGeneratorMock struct {
}

func (e *ErrorGeneratorMock) Token(ctx context.Context) (*oauth2.Token, error) {
	return nil, errors.New("error for testing")
}

func TestGetTccRule(t *testing.T) {
	r := &RegfeedRulesClient{
		GrpcClient: &grpcclient.GrpcClient{},
	}
	ctx := context.Background()
	tccRule, err := r.GetTccRule(ctx, "0")
	assert.TccRuleNil(tccRule, t)
	assert.ErrNotNil(err, t)
	r.FakeToken = "fake_token"
	tccRule, err = r.GetTccRule(ctx, "10")
	assert.TccRuleNil(tccRule, t)
	assert.ErrNotNil(err, t)
	r.OAuth2ClientCredentialsConfig = &ErrorGeneratorMock{}
	tccRule, err = r.GetTccRule(ctx, "10")
	assert.TccRuleNil(tccRule, t)
	assert.ErrNotNil(err, t)
	r.OAuth2ClientCredentialsConfig = &TokenGeneratorMock{}
	tccRule, err = r.GetTccRule(ctx, "10")
	assert.TccRuleNil(tccRule, t)
	assert.ErrNotNil(err, t)
	r.TLS = true
	r.Insecure = true
	tccRule, err = r.GetTccRule(ctx, "10")
	assert.TccRuleNil(tccRule, t)
	assert.ErrNotNil(err, t)
	r.RoundRobin = true
	tccRule, err = r.GetTccRule(ctx, "10")
	assert.TccRuleNil(tccRule, t)
	assert.ErrNotNil(err, t)
	address := "localhost:8888"
	r.Conn = address
	r.TLS = false
	r.RoundRobin = false
	r.OAuth2ClientCredentialsConfig = nil
	lis, err := net.Listen("tcp", address)
	assert.ErrNil(err, t)
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	rules.RegisterRulesServiceServer(grpcServer, &RulesServiceMock{})
	go startGrpcServer(lis, grpcServer, t)
	defer grpcServer.Stop()
	time.Sleep(2 * time.Second)
	tccRule, err = r.GetTccRule(ctx, "10")
	assert.ErrNil(err, t)
	assert.TccRuleNotNil(tccRule, t)
	tccRule, err = r.GetTccRule(ctx, "3")
	assert.ErrNotNil(err, t)
	assert.TccRuleNil(tccRule, t)
	tccRule, err = r.GetTccRule(ctx, "1")
	assert.ErrNotNil(err, t)
	assert.TccRuleNil(tccRule, t)
}

func startGrpcServer(lis net.Listener, grpcServer *grpc.Server, t *testing.T) {
	err := grpcServer.Serve(lis)
	assert.ErrNil(err, t)
}
