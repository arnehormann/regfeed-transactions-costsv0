package rules

import (
	"context"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/golang/protobuf/proto"

	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	rules "infrontfinance.dev/regfeed/grpc/rules/v2"

	grpcclient "infrontfinance.dev/regfeed/transactions-costsv0/grpc"
)

// RegfeedRulesClient is a simple struct that helps accessing the rules service
type RegfeedRulesClient struct {
	*grpcclient.GrpcClient
}

// GetTccRule returns the Transaction Cost Calculation set of rules for the specific asset class
func (r *RegfeedRulesClient) GetTccRule(ctx context.Context, productCategoryId string) (*rules.TccRule, error) {
	l := log.New(os.Stdout, "[RegfeedRulesClient.GetTccRule] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	if productCategoryId == "" {
		l.Printf("Error: product category id is not set")
		return nil, status.Error(codes.InvalidArgument, "product category id is not set")
	}
	conn, err := r.OpenConnection(ctx)
	if err != nil {
		l.Printf("Error: %v", err)
		return nil, status.Error(codes.Unavailable, err.Error())
	}
	defer conn.Close()
	client := rules.NewRulesServiceClient(conn)
	tries := 0
	backoff := int64(1000)
	var response *rules.ListTccRulesResponse
	request := &rules.ListTccRulesRequest{
		ProductCategoryId: []string{productCategoryId},
		Fields: &field_mask.FieldMask{
			Paths: []string{"tcc_rule.id", "tcc_rule.tenant", "tcc_rule.spread_threshold", "tcc_rule.code", "tcc_rule.name",
				"tcc_rule.product_category_id", "tcc_rule.half_spread", "tcc_rule.tolerance_level", "tcc_rule.time_interval",
				"tcc_rule.has_min_price_quality_check", "tcc_rule.min_price_quality", "tcc_rule.min_price_quality_type"},
		},
	}
	callOpts, err := r.GetCallOptions(ctx)
	if err != nil {
		l.Printf("Error: %v", err)
		return nil, status.Error(codes.Unavailable, err.Error())
	}
	for retry := true; retry; retry = tries < 3 && err != nil {
		tries += 1
		if tries > 1 {
			l.Printf("Error: %v\nretrying request to GetTccRule", err)
			jitter := rand.Int63n(backoff)
			sleep := backoff + jitter
			time.Sleep(time.Duration(sleep) * time.Millisecond)
			backoff = backoff * 2
		}
		response, err = client.ListTccRules(ctx, request, callOpts...)
	}
	if err != nil {
		l.Printf("Error: %v", err)
		return nil, status.Errorf(codes.Unavailable, "Unable to send request to rules service: %v", err)
	}
	if response == nil {
		errMsg := "Nil response from the rules service"
		l.Printf(errMsg)
		return nil, status.Errorf(codes.Unavailable, errMsg)
	}
	l.Printf("Request to ListTccRules: %v, response: %v", proto.MarshalTextString(request), proto.MarshalTextString(response))
	if len(response.GetXData()) == 0 {
		errMsg := "No data present in the ListTccRuleResponse's payload"
		l.Printf(errMsg)
		return nil, status.Errorf(codes.Unavailable, errMsg)
	}
	return response.GetXData()[0].GetTccRule(), nil
}
