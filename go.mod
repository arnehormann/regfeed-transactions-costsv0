module infrontfinance.dev/regfeed/transactions-costsv0

go 1.16

require (
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	google.golang.org/grpc v1.29.1
	infrontfinance.dev/iq/env v1.2.0
	infrontfinance.dev/iq/iqrpc v0.0.0-20200918110734-0dd46a9bbfe9
)
