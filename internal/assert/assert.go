package assert

import (
	"fmt"
	"math"
	"strings"
	"testing"

	tc "infrontfinance.dev/regfeed/data/transactioncosts"
	rules "infrontfinance.dev/regfeed/grpc/rules/v2"
	security "infrontfinance.dev/regfeed/grpc/security/v2"
)

func False(value bool, t *testing.T) {
	t.Helper()
	if value {
		t.Error("got true, want false")
	}
}

func ErrNil(err error, t *testing.T) {
	t.Helper()
	if err != nil {
		t.Error("got non nil error, want nil error")
	}
}

func ErrNotNil(err error, t *testing.T) {
	t.Helper()
	if err == nil {
		t.Error("got nil error, want non nil error")
	}
}

func ErrMsg(errMsg string, err error, t *testing.T) {
	t.Helper()
	if !strings.Contains(fmt.Sprintf("%v", err), errMsg) {
		t.Error("got error msg: ", err, ", want error msg:", errMsg)
	}
}

func OutputNotNil(o *tc.Output, t *testing.T) {
	t.Helper()
	if o == nil {
		t.Error("got nil, want non nil")
	}
}

func NilCosts(costs *tc.Costs, t *testing.T) {
	t.Helper()
	if costs != nil {
		t.Error("go non nil, want nil")
	}
}

func True(value bool, t *testing.T) {
	t.Helper()
	if !value {
		t.Error("got false, want true")
	}
}

func BoolPointerNil(value *bool, t *testing.T) {
	t.Helper()
	if value != nil {
		t.Error("got non nil, want nil")
	}
}

func String(expected, real string, t *testing.T) {
	t.Helper()
	if expected != real {
		t.Error("got ", real, ", want ", expected)
	}
}

func NotNilTccInput(input *tc.Input, t *testing.T) {
	t.Helper()
	if input == nil {
		t.Error("got nil, want non nil")
	}
}

func Uint32(expected, real uint32, t *testing.T) {
	t.Helper()
	if expected != real {
		t.Error("got:", real, ", want:", expected)
	}
}

func Float64(expected, real float64, t *testing.T) {
	t.Helper()
	if math.Abs(expected-real) > 0.00001 {
		t.Error("got:", real, ", want:", expected)
	}
}

func OrderType(expected, real tc.Order_Type, t *testing.T) {
	t.Helper()
	if expected != real {
		t.Error("got:", real, ", want:", expected)
	}
}

func Costs(expected, real *tc.Costs, t *testing.T) {
	Float64(expected.GetAbsolute(), real.GetAbsolute(), t)
	Float64(expected.GetPercent(), real.GetPercent(), t)
}

func TccRuleNotNil(tccRule *rules.TccRule, t *testing.T) {
	t.Helper()
	if tccRule == nil {
		t.Error("got nil TccRule, want non nil TccRule")
	}
}

func TccRuleNil(tccRule *rules.TccRule, t *testing.T) {
	t.Helper()
	if tccRule != nil {
		t.Error("got non nil TccRule, want nil TccRule")
	}
}

func SecDataSliceNil(s []*security.SecuritiesData, t *testing.T) {
	t.Helper()
	if s != nil {
		t.Error("got non nil slice, want nil")
	}
}

func GetSecuritiesRequestNil(g *security.GetSecuritiesRequest, t *testing.T) {
	t.Helper()
	if g != nil {
		t.Error("got non nil GetSecuritiesRequest, want nil")
	}
}

func Int32(expected, real int32, t *testing.T) {
	t.Helper()
	if expected != real {
		t.Error("got:", real, ", want:", expected)
	}
}

func SecDataSliceNotNil(s []*security.SecuritiesData, t *testing.T) {
	t.Helper()
	if s == nil {
		t.Error("got nil slice, want non nil")
	}
}

func SecDataNotNil(s *security.SecuritiesData, t *testing.T) {
	t.Helper()
	if s == nil {
		t.Error("got nil SecuritiesData, want non nil")
	}
}

func HistoryQuotesDataSliceNil(s []*security.HistoryQuotesData, t *testing.T) {
	t.Helper()
	if s != nil {
		t.Error("got non nil slice, want nil")
	}
}

func HistoryQuotesDataSliceNotNil(s []*security.HistoryQuotesData, t *testing.T) {
	t.Helper()
	if s == nil {
		t.Error("got nil slice, want non nil")
	}
}

func HistoryQuotesDataNotNil(s *security.HistoryQuotesData, t *testing.T) {
	t.Helper()
	if s == nil {
		t.Error("got nil HistoryQuoteData, want non nil")
	}
}

func TickQuotesDataSliceNil(s []*security.TickQuotesData, t *testing.T) {
	t.Helper()
	if s != nil {
		t.Error("got non nil slice, want nil")
	}
}

func TickQuotesDataSliceNotNil(s []*security.TickQuotesData, t *testing.T) {
	t.Helper()
	if s == nil {
		t.Error("got nil slice, want non nil")
	}
}

func TickQuotesDataNotNil(s *security.TickQuotesData, t *testing.T) {
	t.Helper()
	if s == nil {
		t.Error("got nil TickQuoteData, want non nil")
	}
}
