package security

import (
	"context"
	"errors"

	security "infrontfinance.dev/regfeed/grpc/security/v2"
)

type SecurityServiceMock struct {
	security.SecurityServiceServer
	ReturnError bool
}

func (s *SecurityServiceMock) GetSecurities(ctx context.Context, in *security.GetSecuritiesRequest) (*security.Securities, error) {
	if s.ReturnError {
		return nil, errors.New("mock test error")
	}
	return &security.Securities{
		XData: []*security.SecuritiesData{
			&security.SecuritiesData{},
		},
	}, nil
}

func (s *SecurityServiceMock) GetHistoryQuotes(ctx context.Context, in *security.GetHistoryQuotesRequest) (*security.HistoryQuotes, error) {
	if s.ReturnError {
		return nil, errors.New("mock test error")
	}
	return &security.HistoryQuotes{
		XData: []*security.HistoryQuotesData{
			&security.HistoryQuotesData{},
		},
	}, nil
}

func (s *SecurityServiceMock) GetTickQuotes(ctx context.Context, in *security.GetTickQuotesRequest) (*security.TickQuotes, error) {
	if s.ReturnError {
		return nil, errors.New("mock test error")
	}
	return &security.TickQuotes{
		XData: []*security.TickQuotesData{
			&security.TickQuotesData{},
		},
	}, nil
}
