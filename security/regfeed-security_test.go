package security

import (
	"context"
	"net"
	"testing"
	"time"

	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc"

	security "infrontfinance.dev/regfeed/grpc/security/v2"
	assert "infrontfinance.dev/regfeed/transactions-costsv0/assert"
	grpcclient "infrontfinance.dev/regfeed/transactions-costsv0/grpc"
)

const (
	vwdKey        = "965239.DTB.4100.1I_1"
	isin          = "LU0249332452"
	wkn           = "A0KE4X"
	currency      = "EUR"
	quotationType = int32(security.QuotationCode_UNIT)
)

func getInput() *GetSecuritiesInput {
	return &GetSecuritiesInput{
		VwdKey:        vwdKey,
		QuotationType: proto.Int32(quotationType),
		Currency:      currency,
	}
}

func TestBuildGetSecuritiesRequest(t *testing.T) {
	req, err := BuildGetSecuritiesRequest(nil)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("GetSecuritiesInput is nil", err, t)
	assert.GetSecuritiesRequestNil(req, t)
	input := getInput()
	req, err = BuildGetSecuritiesRequest(input)
	assert.ErrNil(err, t)
	assert.Int32(100, *req.Limit, t)
	assert.String(vwdKey, req.InstrumentKey[0], t)
	assert.False(*req.SearchArbitrageLogic, t)
	assert.String("FUT", req.NotSecurityCategoryCode[0], t)
	assert.String("OPT", req.NotSecurityCategoryCode[1], t)
	assert.String(currency, req.CurrencyIsoalpha3[0], t)
	assert.Int32(quotationType, req.QuotationId[0], t)
	input.Isin = isin
	input.VwdKey = ""
	req, err = BuildGetSecuritiesRequest(input)
	assert.ErrNil(err, t)
	assert.String(isin, req.Isin[0], t)
	assert.True(*req.SearchArbitrageLogic, t)
	input.Wkn = wkn
	input.Isin = ""
	req, err = BuildGetSecuritiesRequest(input)
	assert.ErrNil(err, t)
	assert.String(wkn, req.Localcode[0], t)
	assert.True(*req.SearchArbitrageLogic, t)
}

func TestGetSecurities(t *testing.T) {
	s := &RegfeedSecurityClient{}
	ctx := context.Background()
	// nil *GetSecuritiesInput, error expected
	securities, err := s.GetSecurities(ctx, nil)
	assert.SecDataSliceNil(securities, t)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("GetSecuritiesInput is nil", err, t)
	// RegfeedSecurityClient has a nil GrpcClient, error expected
	securities, err = s.GetSecurities(ctx, getInput())
	assert.SecDataSliceNil(securities, t)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("GrpcClient was not initialized", err, t)
	// Using a mock grpc server for the security service, error expected
	address := "localhost:8886"
	s.GrpcClient = &grpcclient.GrpcClient{
		Conn:      address,
		FakeToken: "dummy_token",
	}
	lis, err := net.Listen("tcp", address)
	assert.ErrNil(err, t)
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	mock := &SecurityServiceMock{
		ReturnError: true,
	}
	security.RegisterSecurityServiceServer(grpcServer, mock)
	go startGrpcServer(lis, grpcServer, t)
	defer grpcServer.Stop()
	time.Sleep(2 * time.Second)
	securities, err = s.GetSecurities(ctx, getInput())
	assert.SecDataSliceNil(securities, t)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("mock test error", err, t)
	// Using a mock grpc server for the security service, error not expected
	mock.ReturnError = false
	securities, err = s.GetSecurities(ctx, getInput())
	assert.SecDataSliceNotNil(securities, t)
	assert.SecDataNotNil(securities[0], t)
	assert.ErrNil(err, t)
}

func TestGetHistoryQuotes(t *testing.T) {
	s := &RegfeedSecurityClient{}
	ctx := context.Background()
	// nil *GetHistoryQuotesRequest, error expected
	historyQuotes, err := s.GetHistoryQuotes(ctx, nil)
	assert.HistoryQuotesDataSliceNil(historyQuotes, t)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("GetHistoryQuotesRequest is nil", err, t)
	// RegfeedSecurityClient has a nil GrpcClient, error expected
	historyQuotes, err = s.GetHistoryQuotes(ctx, &security.GetHistoryQuotesRequest{})
	assert.HistoryQuotesDataSliceNil(historyQuotes, t)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("GrpcClient was not initialized", err, t)
	// Using a mock grpc server for the security service, error expected
	address := "localhost:8887"
	s.GrpcClient = &grpcclient.GrpcClient{
		Conn:      address,
		FakeToken: "dummy_token",
	}
	lis, err := net.Listen("tcp", address)
	assert.ErrNil(err, t)
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	mock := &SecurityServiceMock{
		ReturnError: true,
	}
	security.RegisterSecurityServiceServer(grpcServer, mock)
	go startGrpcServer(lis, grpcServer, t)
	defer grpcServer.Stop()
	time.Sleep(2 * time.Second)
	historyQuotes, err = s.GetHistoryQuotes(ctx, &security.GetHistoryQuotesRequest{})
	assert.HistoryQuotesDataSliceNil(historyQuotes, t)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("mock test error", err, t)
	// Using a mock grpc server for the security service, error not expected
	mock.ReturnError = false
	historyQuotes, err = s.GetHistoryQuotes(ctx, &security.GetHistoryQuotesRequest{})
	assert.HistoryQuotesDataSliceNotNil(historyQuotes, t)
	assert.HistoryQuotesDataNotNil(historyQuotes[0], t)
	assert.ErrNil(err, t)
}

func TestGetTickQuotes(t *testing.T) {
	s := &RegfeedSecurityClient{}
	ctx := context.Background()
	// nil *GetTickQuotesRequest, error expected
	tickQuotes, err := s.GetTickQuotes(ctx, nil)
	assert.TickQuotesDataSliceNil(tickQuotes, t)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("GetTickQuotesRequest is nil", err, t)
	// RegfeedSecurityClient has a nil GrpcClient, error expected
	tickQuotes, err = s.GetTickQuotes(ctx, &security.GetTickQuotesRequest{})
	assert.TickQuotesDataSliceNil(tickQuotes, t)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("GrpcClient was not initialized", err, t)
	// Using a mock grpc server for the security service, error expected
	address := "localhost:8881"
	s.GrpcClient = &grpcclient.GrpcClient{
		Conn:      address,
		FakeToken: "dummy_token",
	}
	lis, err := net.Listen("tcp", address)
	assert.ErrNil(err, t)
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	mock := &SecurityServiceMock{
		ReturnError: true,
	}
	security.RegisterSecurityServiceServer(grpcServer, mock)
	go startGrpcServer(lis, grpcServer, t)
	defer grpcServer.Stop()
	time.Sleep(2 * time.Second)
	tickQuotes, err = s.GetTickQuotes(ctx, &security.GetTickQuotesRequest{})
	assert.TickQuotesDataSliceNil(tickQuotes, t)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("mock test error", err, t)
	// Using a mock grpc server for the security service, error not expected
	mock.ReturnError = false
	tickQuotes, err = s.GetTickQuotes(ctx, &security.GetTickQuotesRequest{})
	assert.TickQuotesDataSliceNotNil(tickQuotes, t)
	assert.TickQuotesDataNotNil(tickQuotes[0], t)
	assert.ErrNil(err, t)
}

func startGrpcServer(lis net.Listener, grpcServer *grpc.Server, t *testing.T) {
	err := grpcServer.Serve(lis)
	assert.ErrNil(err, t)
}
