package security

import (
	"context"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	security "infrontfinance.dev/regfeed/grpc/security/v2"
	grpcclient "infrontfinance.dev/regfeed/transactions-costsv0/grpc"
)

// RegfeedSecurityClient is a simple struct that helps to access the security service
type RegfeedSecurityClient struct {
	*grpcclient.GrpcClient
}

// GetSecuritiesInput encapsulates the necessary information to search for assets
type GetSecuritiesInput struct {
	VwdKey        string
	Isin          string
	Wkn           string
	Currency      string
	QuotationType *int32
	Derivative    bool
}

// GetSecurities calls the security service's GetSecurities endpoint
func (s *RegfeedSecurityClient) GetSecurities(ctx context.Context, g *GetSecuritiesInput) ([]*security.SecuritiesData, error) {
	l := log.New(os.Stdout, "[RegfeedSecurityClient.GetSecurities] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	if g == nil {
		return nil, status.Error(codes.InvalidArgument, "GetSecuritiesInput is nil")
	}
	req, err := BuildGetSecuritiesRequest(g)
	if err != nil {
		l.Printf("%v", err)
	}
	conn, err := s.OpenConnection(ctx)
	if err != nil {
		l.Printf("%v", err)
		return nil, err
	}
	defer conn.Close()
	client := security.NewSecurityServiceClient(conn)
	tries := 0
	backoff := int64(1000)
	callOpts, err := s.GetCallOptions(ctx)
	if err != nil {
		l.Printf("%v", err)
		return nil, err
	}
	var securities *security.Securities
	var securityDataSlice []*security.SecuritiesData
	for retry := true; retry; retry = tries < 3 && err != nil {
		tries += 1
		if tries > 1 {
			l.Printf("Error: %v\nretrying request to GetSecurities", err)
			jitter := rand.Int63n(backoff)
			sleep := backoff + jitter
			time.Sleep(time.Duration(sleep) * time.Millisecond)
			backoff = backoff * 2
		}
		securities, err = client.GetSecurities(ctx, req, callOpts...)
	}
	if err == nil {
		securityDataSlice = securities.GetXData()
	}
	return securityDataSlice, err
}

// GetTickQuotes calls the security service's GetTickQuotes endpoint
func (s *RegfeedSecurityClient) GetTickQuotes(ctx context.Context, g *security.GetTickQuotesRequest) ([]*security.TickQuotesData, error) {
	l := log.New(os.Stdout, "[RegfeedSecurityClient.GetTickQuotes] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	if g == nil {
		return nil, status.Error(codes.InvalidArgument, "GetTickQuotesRequest is nil")
	}
	conn, err := s.OpenConnection(ctx)
	if err != nil {
		l.Printf("%v", err)
		return nil, err
	}
	defer conn.Close()
	client := security.NewSecurityServiceClient(conn)
	tries := 0
	backoff := int64(1000)
	callOpts, err := s.GetCallOptions(ctx)
	if err != nil {
		l.Printf("%v", err)
		return nil, err
	}
	var tickQuotes *security.TickQuotes
	var tickQuotesDataSlice []*security.TickQuotesData
	for retry := true; retry; retry = tries < 3 && err != nil {
		tries += 1
		if tries > 1 {
			l.Printf("Error: %v\nretrying request to GetTickQuotes", err)
			jitter := rand.Int63n(backoff)
			sleep := backoff + jitter
			time.Sleep(time.Duration(sleep) * time.Millisecond)
			backoff = backoff * 2
		}
		tickQuotes, err = client.GetTickQuotes(ctx, g, callOpts...)
	}
	if err == nil {
		tickQuotesDataSlice = tickQuotes.GetXData()
	}
	return tickQuotesDataSlice, err
}

// GetHistoryQuotes calls the security service's GetHistoryQuotes endpoint
func (s *RegfeedSecurityClient) GetHistoryQuotes(ctx context.Context, g *security.GetHistoryQuotesRequest) ([]*security.HistoryQuotesData, error) {
	l := log.New(os.Stdout, "[RegfeedSecurityClient.GetHistoryQuotes] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	if g == nil {
		return nil, status.Error(codes.InvalidArgument, "GetHistoryQuotesRequest is nil")
	}
	conn, err := s.OpenConnection(ctx)
	if err != nil {
		l.Printf("%v", err)
		return nil, err
	}
	defer conn.Close()
	client := security.NewSecurityServiceClient(conn)
	tries := 0
	backoff := int64(1000)
	callOpts, err := s.GetCallOptions(ctx)
	if err != nil {
		l.Printf("%v", err)
		return nil, err
	}
	var historyQuotes *security.HistoryQuotes
	var historyQuotesDataSlice []*security.HistoryQuotesData
	for retry := true; retry; retry = tries < 3 && err != nil {
		tries += 1
		if tries > 1 {
			l.Printf("Error: %v\nretrying request to GetHistoryQuotes", err)
			jitter := rand.Int63n(backoff)
			sleep := backoff + jitter
			time.Sleep(time.Duration(sleep) * time.Millisecond)
			backoff = backoff * 2
		}
		historyQuotes, err = client.GetHistoryQuotes(ctx, g, callOpts...)
	}
	if err == nil {
		historyQuotesDataSlice = historyQuotes.GetXData()
	}
	return historyQuotesDataSlice, err
}

// BuildGetSecuritiesRequest builds an instance of GetSecuritiesRequest
func BuildGetSecuritiesRequest(g *GetSecuritiesInput) (*security.GetSecuritiesRequest, error) {
	if g == nil {
		return nil, status.Error(codes.InvalidArgument, "GetSecuritiesInput is nil")
	}
	request := &security.GetSecuritiesRequest{
		Limit: proto.Int32(100),
	}
	switch {
	case g.VwdKey != "":
		request.InstrumentKey = []string{g.VwdKey}
		request.SearchArbitrageLogic = proto.Bool(false)
		break
	case g.Isin != "":
		request.Isin = []string{g.Isin}
		request.SearchArbitrageLogic = proto.Bool(true)
		break
	case g.Wkn != "":
		request.Localcode = []string{g.Wkn}
		request.SearchArbitrageLogic = proto.Bool(true)
	}
	if g.Currency != "" {
		request.CurrencyIsoalpha3 = []string{g.Currency}
	}
	if !g.Derivative {
		request.NotSecurityCategoryCode = []string{"FUT", "OPT"}
	}
	if g.QuotationType != nil {
		request.QuotationId = []int32{*g.QuotationType}
	}
	return request, nil
}
