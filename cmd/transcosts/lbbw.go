package main

import (
	"context"
	"log"
	"math"
	"os"
	"strings"

	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"infrontfinance.dev/regfeed/data/envelope"
	"infrontfinance.dev/regfeed/data/tag"
	tc "infrontfinance.dev/regfeed/data/transactioncosts"
	resultproto "infrontfinance.dev/regfeed/grpc/result/v2"
	res "infrontfinance.dev/regfeed/transactions-costsv0/result"
)

// LbbwAm implements the Tenant interface for the Landesbank Baden-Wuerttemberg Asset Management
type LbbwAm struct {
	exporters          []Exporter
	regfeedRulesClient RulesClient
}

// calculateTransactionCosts encapsulates the main business logic specific to LBBW AM
// for the transaction costs analysis
func (lbbwAm *LbbwAm) calculateTransactionCosts(ctx context.Context, env *envelope.Envelope) error {
	l := log.New(os.Stdout, "[LbbwAm.calculateTransactionCosts] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	data := env.Data
	if len(data) == 0 {
		errMsg := "Error: Envelope has an empty payload, nothing to valuate"
		l.Printf(errMsg)
		return status.Errorf(codes.InvalidArgument, errMsg)
	}
	r := new(tc.Request)
	if err := proto.Unmarshal(data, r); err != nil {
		l.Printf(err.Error())
		return status.Errorf(codes.Internal, err.Error())
	}
	liquid, err := IsLiquid(env.GetMeta())
	if err != nil {
		l.Print(err.Error())
		return status.Errorf(codes.Internal, err.Error())
	}
	o, err := lbbwAm.getTccOutput(ctx, r, liquid)
	if err != nil {
		l.Printf(err.Error())
		return status.Errorf(codes.Internal, err.Error())
	}
	c := &resultproto.CreateDataRequest{
		CustomerInput: res.CreateCustomerInput(r),
		Process:       resultproto.Delivery_TCC,
		List: []*resultproto.CreateList{&resultproto.CreateList{
			Valuation: &resultproto.Valuation{Tcc: o},
		}},
		Tags:             env.GetMeta().GetTag(),
		SourceName:       r.GetProcessInformation().GetFileInformation().GetInputFileName(),
		ReportTimestamp:  r.GetProcessInformation().GetFileInformation().GetReportTimestamp(),
		NumberOfProducts: proto.Int32(r.GetProcessInformation().GetFileInformation().GetNumberOfProducts()),
		ProductGroup:     proto.String(r.GetProcessInformation().GetFileInformation().GetProductGroup()),
		Ref:              env.GetMeta().GetRef(),
	}
	var errMsgs []string
	for _, exporter := range lbbwAm.exporters {
		err = exporter.Export(ctx, c)
		if err != nil {
			l.Printf(err.Error())
			errMsgs = append(errMsgs)
		}
	}
	if errMsgs != nil {
		return status.Errorf(codes.Internal, strings.Join(errMsgs, "\n"))
	}
	return nil
}

// IsLiquid checks whether the TCCVPR tag is present in the envelope metadata.
// If so, the product is liquid, and the service can leverage the
// new priips method.
func IsLiquid(m *envelope.Meta) (bool, error) {
	l := log.New(os.Stdout, "[LbbwAm.IsLiquid] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	if m == nil {
		errMsg := "Envelope metadata is nil"
		l.Printf(errMsg)
		return false, status.Errorf(codes.InvalidArgument, errMsg)
	}
	tags := m.GetTag()
	if len(tags) == 0 {
		errMsg := "Envelope metadata has no tags"
		l.Printf(errMsg)
		return false, status.Errorf(codes.InvalidArgument, errMsg)
	}
	for _, t := range tags {
		if t == tag.Tag_TCCOTC.String() {
			return false, nil
		}
	}
	for _, t := range tags {
		if t == tag.Tag_TCC.String() {
			return true, nil
		}
	}
	errMsg := "No TCC or TCCOTC tag in the envelope's metadata"
	l.Printf(errMsg)
	return false, status.Errorf(codes.InvalidArgument, errMsg)
}

// getTccOutput calculates the tcc output (new priips, full priips, and so on)
func (lbbwAm *LbbwAm) getTccOutput(ctx context.Context, r *tc.Request, liquid bool) (*tc.Output, error) {
	l := log.New(os.Stdout, "[LbbwAm.getTccOutput] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	input := r.GetTransactionCosts()
	nominal := input.GetNominal()
	tccRule, err := lbbwAm.regfeedRulesClient.GetTccRule(ctx, input.GetProductCategoryIdTcc())
	if err != nil {
		l.Printf("Error: %v", err)
		return nil, err
	}
	if tccRule == nil {
		errMsg := "Nil tcc rule from the rules service client"
		l.Printf(errMsg)
		return nil, status.Errorf(codes.Internal, errMsg)
	}
	explicitCosts := input.ExplicitTransactionCosts
	o := &tc.Output{
		ExplicitCosts: createCosts(explicitCosts),
	}
	if liquid {
		orderType := input.OrderType
		arrivalPrice := input.GetArrivalPrice()
		o.ArrivalPrice = arrivalPrice
		netExecutionPrice := input.NetExecutionPrice
		spread := netExecutionPrice - arrivalPrice
		spreadThreshold := tccRule.GetSpreadThreshold()
		if math.Abs(spread) <= spreadThreshold {
			var w float64
			switch orderType {
			case tc.Order_BUY:
				w = -1.0
			default:
				w = 1.0
			}
			implicitCostsFullPriips := w * spread * float64(nominal)
			fullPriipsCosts := implicitCostsFullPriips + explicitCosts
			o.TotalCostsFullPriips = createCosts(fullPriipsCosts)
			o.ImplicitCostsFullPriips = createCosts(implicitCostsFullPriips)
		}
	}
	implicitCostsNewPriips := tccRule.GetHalfSpread() * float64(nominal)
	newPriipsCosts := implicitCostsNewPriips + explicitCosts
	o.TotalCostsNewPriips = createCosts(newPriipsCosts)
	o.ImplicitCostsNewPriips = createCosts(implicitCostsNewPriips)
	return o, nil
}

// createCosts creates an instance of Costs (e.g. explicit, implicit full priips, implicit new priips)
func createCosts(c float64) *tc.Costs {
	return &tc.Costs{
		Absolute: c,
	}
}
