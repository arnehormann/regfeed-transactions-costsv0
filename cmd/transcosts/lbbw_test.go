package main

import (
	"context"
	"testing"

	"github.com/golang/protobuf/proto"

	"infrontfinance.dev/regfeed/data/envelope"
	"infrontfinance.dev/regfeed/data/tag"
	tc "infrontfinance.dev/regfeed/data/transactioncosts"
	result "infrontfinance.dev/regfeed/grpc/result/v2"
	r "infrontfinance.dev/regfeed/grpc/rules/v2"
	assert "infrontfinance.dev/regfeed/transactions-costsv0/assert"
)

const (
	arrivalPrice             = 65.54
	explicitTransactionCosts = 10.0
	netExecutionPrice        = 10.5
	nominal                  = 5
	fundId                   = "1"
	transactionId            = "1"
	currency                 = "EUR"
	orderIssueDate           = "20210315"
	orderExecutionDate       = "20210315"
	orderExecutionTimestamp  = "13:01:30"
	isin                     = "DE0008476250"
	wkn                      = "847625"
	vwdKey                   = "847625.FFM"
	customerCode             = "105A0000001TEST15"
	exchangeRate             = 1.17
	productCategoryId        = 5001
	productCategoryIdTcc     = "5001"
	halfSpread               = 4.9
)

type ExporterMock struct {
	req *result.CreateDataRequest
}

func (e *ExporterMock) Export(ctx context.Context, req *result.CreateDataRequest) error {
	e.req = req
	return nil
}

type RulesClientMock struct {
	spreadThreshold *float64
	returnErr       bool
	returnNil       bool
}

func (mock *RulesClientMock) GetTccRule(ctx context.Context, id string) (*r.TccRule, error) {
	halfSpreadVar := halfSpread
	return &r.TccRule{
		SpreadThreshold: mock.spreadThreshold,
		HalfSpread:      &halfSpreadVar,
	}, nil
}

func getLbbwAmTca(e Exporter, spreadThreshold *float64, returnErr, returnNil bool) *LbbwAm {
	rulesClientMock := &RulesClientMock{spreadThreshold: spreadThreshold}
	return &LbbwAm{
		exporters:          []Exporter{e},
		regfeedRulesClient: rulesClientMock,
	}
}

func getInput(orderType tc.Order_Type) *tc.Input {
	return &tc.Input{
		OrderType:                orderType,
		ArrivalPrice:             proto.Float64(arrivalPrice),
		ExplicitTransactionCosts: explicitTransactionCosts,
		NetExecutionPrice:        netExecutionPrice,
		Nominal:                  nominal,
		ProductCategoryIdTcc:     productCategoryIdTcc,
	}
}

func TestCreateTransactionCosts(t *testing.T) {
	explicitCosts := createCosts(explicitTransactionCosts)
	assert.Float64(explicitTransactionCosts, explicitCosts.Absolute, t)
}

func marshalTcRequest(input *tc.Input, t *testing.T) []byte {
	request := &tc.Request{
		TransactionCosts: input,
	}
	data, err := proto.Marshal(request)
	assert.ErrNil(err, t)
	return data
}

func TestCalculateTransacationCosts(t *testing.T) {
	ctx := context.Background()
	spreadThreshold := 5.0
	exporterMock := &ExporterMock{}
	l := getLbbwAmTca(exporterMock, &spreadThreshold, false, false)
	orderType := tc.Order_BUY
	input := getInput(orderType)
	env := &envelope.Envelope{
		Data: marshalTcRequest(input, t),
		Meta: &envelope.Meta{
			Tag: []string{tag.Tag_TCC.String()},
		},
	}
	err := l.calculateTransactionCosts(ctx, env)
	assert.ErrNil(err, t)
	req := exporterMock.req
	assert.OutputNotNil(req.GetList()[0].GetValuation().GetTcc(), t)
	explicitCosts := createCosts(explicitTransactionCosts)
	assert.Costs(explicitCosts, req.GetList()[0].GetValuation().GetTcc().GetExplicitCosts(), t)
	assert.NilCosts(req.GetList()[0].GetValuation().GetTcc().GetTotalCostsFullPriips(), t)
	assert.NilCosts(req.GetList()[0].GetValuation().GetTcc().GetImplicitCostsFullPriips(), t)
	implicitTransCosts := halfSpread * float64(nominal)
	implicitCosts := createCosts(implicitTransCosts)
	totalTransCosts := calcTotalCosts(implicitTransCosts)
	allCosts := createCosts(totalTransCosts)
	assert.Costs(implicitCosts, req.GetList()[0].GetValuation().GetTcc().GetImplicitCostsNewPriips(), t)
	assert.Costs(allCosts, req.GetList()[0].GetValuation().GetTcc().GetTotalCostsNewPriips(), t)
	spreadThreshold = 100.0
	l = getLbbwAmTca(exporterMock, &spreadThreshold, false, false)
	err = l.calculateTransactionCosts(ctx, env)
	assert.ErrNil(err, t)
	req = exporterMock.req
	assert.OutputNotNil(req.GetList()[0].GetValuation().GetTcc(), t)
	implicitTransCosts = -1.0 * (netExecutionPrice - arrivalPrice) * float64(nominal)
	implicitCosts = createCosts(implicitTransCosts)
	totalTransCosts = calcTotalCosts(implicitTransCosts)
	allCosts = createCosts(totalTransCosts)
	assert.Costs(implicitCosts, req.GetList()[0].GetValuation().GetTcc().GetImplicitCostsFullPriips(), t)
	assert.Costs(allCosts, req.GetList()[0].GetValuation().GetTcc().GetTotalCostsFullPriips(), t)
	sellOrder := tc.Order_SELL
	env.Data = marshalTcRequest(getInput(sellOrder), t)
	err = l.calculateTransactionCosts(ctx, env)
	assert.ErrNil(err, t)
	req = exporterMock.req
	assert.OutputNotNil(req.GetList()[0].GetValuation().GetTcc(), t)
	implicitTransCosts = -1.0 * implicitTransCosts
	implicitCosts = createCosts(implicitTransCosts)
	totalTransCosts = calcTotalCosts(implicitTransCosts)
	allCosts = createCosts(totalTransCosts)
	assert.Costs(implicitCosts, req.GetList()[0].GetValuation().GetTcc().GetImplicitCostsFullPriips(), t)
	assert.Costs(allCosts, req.GetList()[0].GetValuation().GetTcc().GetTotalCostsFullPriips(), t)
	assert.Float64(arrivalPrice, req.GetList()[0].GetValuation().GetTcc().GetArrivalPrice(), t)
}

func calcTotalCosts(c float64) float64 {
	return c + explicitTransactionCosts
}

func TestIsLiquid(t *testing.T) {
	isLiquid, err := IsLiquid(nil)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("Envelope metadata is nil", err, t)
	assert.False(isLiquid, t)
	m := &envelope.Meta{}
	isLiquid, err = IsLiquid(m)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("Envelope metadata has no tags", err, t)
	assert.False(isLiquid, t)
	m.Tag = []string{}
	isLiquid, err = IsLiquid(m)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("Envelope metadata has no tags", err, t)
	assert.False(isLiquid, t)
	m = &envelope.Meta{
		Tag: []string{tag.Tag_TCC.String()},
	}
	isLiquid, err = IsLiquid(m)
	assert.True(isLiquid, t)
	assert.ErrNil(err, t)
	m = &envelope.Meta{
		Tag: []string{tag.Tag_TCCOTC.String()},
	}
	isLiquid, err = IsLiquid(m)
	assert.False(isLiquid, t)
	assert.ErrNil(err, t)
	m = &envelope.Meta{
		Tag: []string{"VPR"},
	}
	isLiquid, err = IsLiquid(m)
	assert.False(isLiquid, t)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("No TCC or TCCOTC tag in the envelope's metadata", err, t)
}
