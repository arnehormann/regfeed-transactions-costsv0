package main

import (
	"context"
	"log"
	"os"

	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"infrontfinance.dev/regfeed/data/envelope"
	r "infrontfinance.dev/regfeed/grpc/rules/v2"
)

// Server implements the valuator interface for a transactions costs analysis service
type Server struct {
	cm     *grpc_prometheus.ClientMetrics
	tenant Tenant
}

// Tenant is an interface for all customer specific implementation for the transaction costs analysis
type Tenant interface {
	calculateTransactionCosts(context.Context, *envelope.Envelope) error
}

// RulesClient is an interface for a rules service client which returns a Transaction Costs Calculation set of rules
type RulesClient interface {
	GetTccRule(context.Context, string) (*r.TccRule, error)
}

// Transaction costs service implements the valuator interface
func (s *Server) Valuate(ctx context.Context, env *envelope.Envelope) (*envelope.Envelope, error) {
	l := log.New(os.Stdout, "[Valuate] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	if s.tenant == nil {
		l.Fatal("Error: Tenant is nil, please define the customer for this instance")
	}
	if env == nil {
		errMsg := "Error: Nil envelope, nothing to valuate"
		l.Printf(errMsg)
		return &envelope.Envelope{}, status.Errorf(codes.InvalidArgument, errMsg)
	}
	err := s.tenant.calculateTransactionCosts(ctx, env)
	if err != nil {
		l.Printf("%v", err)
		return &envelope.Envelope{}, status.Errorf(codes.Internal, err.Error())
	}
	return &envelope.Envelope{}, nil
}
