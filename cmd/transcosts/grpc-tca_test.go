package main

import (
	"context"
	"fmt"
	"testing"

	"infrontfinance.dev/regfeed/data/envelope"
	assert "infrontfinance.dev/regfeed/transactions-costsv0/assert"
)

type TenantMock struct {
	methodCalled bool
}

func (t *TenantMock) calculateTransactionCosts(ctx context.Context, env *envelope.Envelope) error {
	if env.Data == nil {
		return fmt.Errorf("No data present in the envelope")
	}
	t.methodCalled = true
	return nil
}

func TestValuate(t *testing.T) {
	mock := &TenantMock{}
	assert.False(mock.methodCalled, t)
	s := &Server{tenant: mock}
	ctx := context.Background()
	_, err := s.Valuate(ctx, nil)
	assertErrMethodCalledFalse(mock, err, "Nil envelope, nothing to valuate", t)
	env := &envelope.Envelope{}
	_, err = s.Valuate(ctx, env)
	assertErrMethodCalledFalse(mock, err, "No data present in the envelope", t)
	env.Data = []byte("data")
	_, err = s.Valuate(ctx, env)
	assert.ErrNil(err, t)
	assert.True(mock.methodCalled, t)
}

func assertErrMethodCalledFalse(mock *TenantMock, err error, errMsg string, t *testing.T) {
	t.Helper()
	assert.ErrMsg(errMsg, err, t)
	assert.False(mock.methodCalled, t)
}
