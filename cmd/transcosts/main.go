package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"

	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"golang.org/x/oauth2/clientcredentials"

	"infrontfinance.dev/iq/env"
	"infrontfinance.dev/iq/iqrpc"
	"infrontfinance.dev/regfeed/data/valuator"

	grpcclient "infrontfinance.dev/regfeed/transactions-costsv0/grpc"
	result "infrontfinance.dev/regfeed/transactions-costsv0/result"
	rules "infrontfinance.dev/regfeed/transactions-costsv0/rules"
)

func main() {
	var (
		mode string
		args []string
	)
	if len(os.Args) > 1 {
		mode = os.Args[1]
		args = os.Args[2:]
	}
	switch mode {
	case "jsonenv":
		jsonEnv(args...)
		serve()
	default:
		serve()
	}
}

// jsonEnv reads the s3 secrets stored in a json file and sets the corresponding environment variables:
// AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
func jsonEnv(args ...string) {
	l := log.New(os.Stdout, "[jsonEnv] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	var (
		help      = false
		debug     = false
		file      = "reg_s3_secrets.json"
		field2env = "accessKeyId:AWS_ACCESS_KEY_ID,accessKeySecret:AWS_SECRET_ACCESS_KEY"
	)
	self := os.Args[0]
	fs := flag.NewFlagSet("jsonenv", flag.ContinueOnError)
	fs.BoolVar(&debug, "debug", debug, "print keys of enviroment variables before starting the server")
	fs.BoolVar(&help, "help", help, "print this help text")
	fs.StringVar(&file, "file", file, "input json file with a flat object of fieldname:value string tuples to map to environment variables")
	fs.StringVar(&field2env, "map", field2env, "fieldname:envvar tuples or disallowed fields as comma separated entries")
	err := fs.Parse(args)
	if err != nil || help {
		fmt.Fprintf(os.Stderr, "Arguments for %q in swarmwrap mode:\n", self)
		fs.PrintDefaults()
		if err != nil {
			l.Fatalf("Flag parsing error: %v\n", err)
		}
		return
	}
	data, err := ioutil.ReadFile(file)
	if err != nil {
		l.Fatalf("Could not read file %q: %v\n", file, err)
	}
	var fields map[string]string
	err = json.Unmarshal(data, &fields)
	if err != nil {
		l.Fatalf("Could not read fields from file %q: %v", file, err)
	}
	remap := make(map[string]string)
	if field2env != "" {
		tuples := strings.Split(field2env, ",")
		for _, rawkv := range tuples {
			kv := strings.SplitN(rawkv, ":", 2)
			k, v := kv[0], ""
			if len(kv) == 2 {
				v = kv[1]
			}
			remap[k] = v
		}
	}
	for k, v := range fields {
		if envname, ok := remap[k]; ok {
			if envname == "" {
				continue
			}
			k = envname
			os.Setenv(k, v)
		}
	}
	if debug {
		var envvars []string
		for _, e := range os.Environ() {
			envvars = append(envvars, strings.SplitN(e, "=", 2)[0])
		}
		sort.Strings(envvars)
		l.Printf("Environment variables: %v\n", envvars)
	}
}

// serve starts the gRPC server for the transaction costs service
func serve() {
	l := log.New(os.Stdout, "[serve] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	cfg := env.NewConfig("TCC_")

	rulesClient := &rules.RegfeedRulesClient{
		GrpcClient: &grpcclient.GrpcClient{
			Conn:       cfg.Env("RULES_SERVICE", ""),
			TLS:        cfg.Bool("RULES_SERVICE_TLS", false),
			Insecure:   cfg.Bool("RULES_SERVICE_TLS_INSECURE", false),
			FakeToken:  cfg.Env("RULES_SERVICE_FAKE_OAUTH_TOKEN", ""),
			RoundRobin: cfg.Bool("RULES_SERVICE_ROUND_ROBIN", true),
		},
	}

	rulesClientCreds := &clientcredentials.Config{
		ClientID:     cfg.Env("RULES_SERVICE_CLIENT_ID", ""),
		ClientSecret: cfg.Env("RULES_SERVICE_CLIENT_SECRET", ""),
		TokenURL:     cfg.Env("RULES_SERVICE_OAUTH_TOKEN_URL", ""),
		Scopes:       strings.Split(cfg.Env("RULES_SERVICE_OAUTH_SCOPES", ""), ","),
	}

	if rulesClientCreds.ClientID != "" {
		rulesClient.OAuth2ClientCredentialsConfig = rulesClientCreds
	}

	if rulesClient.Conn == "" {
		l.Fatalf("Endpoint for the rules service was not defined")
	}

	resultClient := &result.RegfeedResultClient{
		GrpcClient: &grpcclient.GrpcClient{
			Conn:       cfg.Env("RESULT_SERVICE", ""),
			TLS:        cfg.Bool("RESULT_SERVICE_TLS", false),
			Insecure:   cfg.Bool("RESULT_SERVICE_TLS_INSECURE", false),
			FakeToken:  cfg.Env("RESULT_SERVICE_FAKE_OAUTH_TOKEN", ""),
			RoundRobin: cfg.Bool("RESULT_SERVICE_ROUND_ROBIN", true),
		},
	}

	resultClientCreds := &clientcredentials.Config{
		ClientID:     cfg.Env("RESULT_SERVICE_CLIENT_ID", ""),
		ClientSecret: cfg.Env("RESULT_SERVICE_CLIENT_SECRET", ""),
		TokenURL:     cfg.Env("RESULT_SERVICE_OAUTH_TOKEN_URL", ""),
		Scopes:       strings.Split(cfg.Env("RESULT_SERVICE_OAUTH_SCOPES", ""), ","),
	}

	if resultClientCreds.ClientID != "" {
		resultClient.OAuth2ClientCredentialsConfig = resultClientCreds
	}

	if resultClient.Conn == "" {
		l.Fatalf("Endpoint for the result service was not defined")
	}

	tenantId := cfg.Env("TENANT", "lbbw-am")
	var t Tenant
	switch strings.ToLower(tenantId) {
	case "lbbw-am":
		t = &LbbwAm{exporters: []Exporter{resultClient}, regfeedRulesClient: rulesClient}
	//LBBW-AM is the only customer for this project at the moment
	default:
		l.Fatalf("No tenant defined")
	}
	tcaService := &Server{tenant: t}
	grpcServer, err := iqrpc.NewServer(cfg, 10000)
	if err != nil {
		l.Fatalf("Error initializing transaction costs grpc server: %v", err)
	}
	valuator.RegisterValuatorServer(grpcServer.Server, tcaService)
	cm := grpc_prometheus.NewClientMetrics()
	grpcServer.Metrics.MustRegister(cm)
	tcaService.cm = cm
	ctx := context.Background()
	cfg.PrintEnvVars(l.Writer())
	if err := grpcServer.StartAndWait(ctx); err != nil {
		l.Fatalf("Error while serving gRPC: %v\n", err)
	}
}
