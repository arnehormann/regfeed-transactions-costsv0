package main

import (
	"context"

	result "infrontfinance.dev/regfeed/grpc/result/v2"
)

// Exporter interface. An exporter can export the result object in many different
// ways. A not limited list of examples are the following ones:
// send the Result object to a downstream service (e.g. regfeed results service);
// store it locally (e.g. in a csv file);
// persist it in a sql database;
// store it as a blob in an S3 bucket.
type Exporter interface {
	Export(context.Context, *result.CreateDataRequest) error
}
