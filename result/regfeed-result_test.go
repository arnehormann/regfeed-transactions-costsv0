package result

import (
	"context"
	"fmt"
	"net"
	"testing"
	"time"

	"github.com/golang/protobuf/proto"

	"golang.org/x/oauth2"
	"google.golang.org/grpc"
	product "infrontfinance.dev/regfeed/data/productv0"
	tc "infrontfinance.dev/regfeed/data/transactioncosts"
	result "infrontfinance.dev/regfeed/grpc/result/v2"

	assert "infrontfinance.dev/regfeed/transactions-costsv0/assert"
	grpcclient "infrontfinance.dev/regfeed/transactions-costsv0/grpc"
)

func TestMapQuotationType(t *testing.T) {
	t.Helper()
	quotationType := mapQuotationType(product.BasicData_UNKNOWNQuotationType)
	if quotationType != result.CustomerInput_UNSPECIFIED {
		t.Error("got:", quotationType, ", want", result.CustomerInput_UNSPECIFIED)
	}
	quotationType = mapQuotationType(99999)
	if quotationType != result.CustomerInput_UNSPECIFIED {
		t.Error("got:", quotationType, ", want:", result.CustomerInput_UNSPECIFIED)
	}
	quotationType = mapQuotationType(product.BasicData_Unit)
	if quotationType != result.CustomerInput_UNIT {
		t.Error("got:", quotationType, ", want:", result.CustomerInput_UNIT)
	}
	quotationType = mapQuotationType(product.BasicData_Percentage)
	if quotationType != result.CustomerInput_PERCENT {
		t.Error("got:", quotationType, ", want:", result.CustomerInput_PERCENT)
	}
	quotationType = mapQuotationType(product.BasicData_Permille)
	if quotationType != result.CustomerInput_PER_MILLE {
		t.Error("got:", quotationType, ", want:", result.CustomerInput_PER_MILLE)
	}
	quotationType = mapQuotationType(product.BasicData_Points)
	if quotationType != result.CustomerInput_POINT {
		t.Error("got:", quotationType, ", want:", result.CustomerInput_POINT)
	}
	quotationType = mapQuotationType(product.BasicData_Other)
	if quotationType != result.CustomerInput_OTHER {
		t.Error("got:", quotationType, ", want:", result.CustomerInput_OTHER)
	}
}

func TestCreateCustomerInput(t *testing.T) {
	arrivalPrice := 10.0
	isin := "DE0007100000"
	wkn := "710000"
	vwdKey := "710000.FFM"
	customerCode := "DAIMLER7100000"
	currency := "EUR"
	isClean := true
	tcc := &tc.Input{
		ArrivalPrice: proto.Float64(arrivalPrice),
	}
	productIdentifier := &product.ProductIdentifier{
		Identifier: &product.Identifier{
			Isin:           isin,
			Wkn:            wkn,
			Vwd:            vwdKey,
			CustomIssuerId: customerCode,
		},
	}
	basicData := &product.BasicData{
		QuotationType: product.BasicData_Unit,
		ProductCurrency: &product.Currency{
			IsoCode: currency,
		},
		IsCleanPrice: isClean,
	}
	req := &tc.Request{
		TransactionCosts:  tcc,
		ProductIdentifier: productIdentifier,
		BasicData:         basicData,
	}
	customerInput := CreateCustomerInput(req)
	assert.NotNilTccInput(customerInput.Tcc, t)
	assert.Float64(arrivalPrice, *customerInput.Tcc.ArrivalPrice, t)
	assert.String(isin, customerInput.Isin, t)
	assert.String(wkn, customerInput.Wpk, t)
	assert.String(vwdKey, customerInput.VwdKey, t)
	assert.String(customerCode, customerInput.CustomerCode, t)
	assert.String(currency, customerInput.TradeCurrencyIsoalpha3, t)
	assert.True(*customerInput.IsCleanPrice, t)
	if result.CustomerInput_UNIT != customerInput.TradeQuotationType {
		t.Error("Expected UNIT as trade quotation type")
	}
}

type ErrorTokenGenerator struct{}

func (e *ErrorTokenGenerator) Token(ctx context.Context) (*oauth2.Token, error) {
	return nil, fmt.Errorf("Error for unit test")
}

type DummyTokenGenerator struct{}

func (d *DummyTokenGenerator) Token(ctx context.Context) (*oauth2.Token, error) {
	return &oauth2.Token{}, nil
}

func TestExport(t *testing.T) {
	r := &RegfeedResultClient{
		GrpcClient: &grpcclient.GrpcClient{},
	}
	ctx := context.Background()
	err := r.Export(ctx, nil)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("create data request is nil", err, t)
	c := &result.CreateDataRequest{}
	err = r.Export(ctx, c)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("transport: Error while dialing dial tcp: missing address", err, t)
	r.OAuth2ClientCredentialsConfig = &ErrorTokenGenerator{}
	err = r.Export(ctx, c)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("Error for unit test", err, t)
	r.OAuth2ClientCredentialsConfig = &DummyTokenGenerator{}
	r.TLS = true
	r.Insecure = true
	r.RoundRobin = true
	err = r.Export(ctx, c)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("transport: Error while dialing dial tcp: missing address", err, t)
	address := "localhost:8885"
	r.Conn = address
	r.OAuth2ClientCredentialsConfig = nil
	r.FakeToken = "dummy_token"
	r.TLS = false
	r.RoundRobin = false
	lis, err := net.Listen("tcp", address)
	assert.ErrNil(err, t)
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	mock := &ResultServiceMock{}
	result.RegisterResultServiceServer(grpcServer, mock)
	go startGrpcServer(lis, grpcServer, t)
	defer grpcServer.Stop()
	time.Sleep(2 * time.Second)
	err = r.Export(ctx, c)
	assert.ErrNil(err, t)
	mock.SendError = true
	err = r.Export(ctx, c)
	assert.ErrNotNil(err, t)
	assert.ErrMsg("Dummy Error", err, t)
}

func startGrpcServer(lis net.Listener, grpcServer *grpc.Server, t *testing.T) {
	err := grpcServer.Serve(lis)
	assert.ErrNil(err, t)
}
