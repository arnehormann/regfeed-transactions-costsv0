package result

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/golang/protobuf/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	product "infrontfinance.dev/regfeed/data/productv0"
	tc "infrontfinance.dev/regfeed/data/transactioncosts"
	result "infrontfinance.dev/regfeed/grpc/result/v2"

	grpcclient "infrontfinance.dev/regfeed/transactions-costsv0/grpc"
)

// RegfeedResultClient is a simple struct that helps accessing the result service
type RegfeedResultClient struct {
	*grpcclient.GrpcClient
}

// This function creates an instance of the result service's CustomerInput based on the transaction costs request
func CreateCustomerInput(r *tc.Request) *result.CustomerInput {
	customerInput := &result.CustomerInput{
		Tcc:                    r.GetTransactionCosts(),
		Isin:                   r.GetProductIdentifier().GetIdentifier().GetIsin(),
		Wpk:                    r.GetProductIdentifier().GetIdentifier().GetWkn(),
		VwdKey:                 r.GetProductIdentifier().GetIdentifier().GetVwd(),
		CustomerCode:           r.GetProductIdentifier().GetIdentifier().GetCustomIssuerId(),
		TradeQuotationType:     mapQuotationType(r.GetBasicData().GetQuotationType()),
		TradeCurrencyIsoalpha3: r.GetBasicData().GetProductCurrency().GetIsoCode(),
		IsCleanPrice:           proto.Bool(r.GetBasicData().GetIsCleanPrice()),
	}
	return customerInput
}

// This function maps the quotation type enum from productv0.proto to the quotation type enum from result service's
// message-v2.proto
func mapQuotationType(q product.BasicData_QuotationType) result.CustomerInput_QuotationType {
	switch q {
	case product.BasicData_UNKNOWNQuotationType:
		return result.CustomerInput_UNSPECIFIED
	case product.BasicData_Unit:
		return result.CustomerInput_UNIT
	case product.BasicData_Percentage:
		return result.CustomerInput_PERCENT
	case product.BasicData_Permille:
		return result.CustomerInput_PER_MILLE
	case product.BasicData_Points:
		return result.CustomerInput_POINT
	case product.BasicData_Other:
		return result.CustomerInput_OTHER
	default:
		return result.CustomerInput_UNSPECIFIED
	}
}

// Creates a new result in the result service database
func (r *RegfeedResultClient) Export(ctx context.Context, c *result.CreateDataRequest) error {
	l := log.New(os.Stdout, "[RegfeedResultClient.Export] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	if r == nil {
		return nil
	}
	if c == nil {
		errMsg := "create data request is nil"
		l.Printf(errMsg)
		return status.Errorf(codes.InvalidArgument, errMsg)
	}
	conn, err := r.OpenConnection(ctx)
	if err != nil {
		l.Printf("Error: %v", err)
		return status.Error(codes.Unavailable, fmt.Sprintf("%v", err))
	}
	defer conn.Close()
	client := result.NewResultServiceClient(conn)
	tries := 0
	backoff := int64(1000)
	callOpts, err := r.GetCallOptions(ctx)
	if err != nil {
		l.Printf("Error: %v", err)
		return status.Error(codes.Unavailable, fmt.Sprintf("%v", err))
	}
	l.Printf("Request to the results service: %v", proto.MarshalTextString(c))
	for retry := true; retry; retry = tries < 3 && err != nil {
		tries += 1
		if tries > 1 {
			l.Printf("Error: %v\nretrying request to CreateData", err)
			jitter := rand.Int63n(backoff)
			sleep := backoff + jitter
			time.Sleep(time.Duration(sleep) * time.Millisecond)
			backoff = backoff * 2
		}
		_, err = client.CreateData(ctx, c, callOpts...)
	}
	return err
}
