package result

import (
	"context"
	"fmt"

	result "infrontfinance.dev/regfeed/grpc/result/v2"
)

type ResultServiceMock struct {
	result.ResultServiceServer
	SendError bool
}

func (r *ResultServiceMock) CreateData(ctx context.Context, request *result.CreateDataRequest) (*result.CreateDataResponse, error) {
	if request == nil {
		return nil, nil
	}
	if r.SendError {
		return nil, fmt.Errorf("Dummy Error")
	}
	return &result.CreateDataResponse{}, nil
}
