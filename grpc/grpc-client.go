package main

import (
	"context"
	"crypto/tls"
	"log"
	"os"

	"golang.org/x/oauth2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/oauth"
	"google.golang.org/grpc/status"
)

type GrpcClient struct {
	// Service endpoint
	Conn string
	// TLS marks whether the connection to the service should use TLS (default: false)
	TLS bool
	// Insecure marks whether the TLS connection to the service should skip certificate
	// validation. (Only has an effect if TLS is true; is necessary at the moment because we still
	// struggle to interconnect different deployment platforms orderly, in this case k8s->docker swarm)
	Insecure bool
	// Fake token that can be used if no OAuth2 identity provider is at hand.
	// It will be cast as a FixedBearerToken.
	// This can be used for development and is overridden by tokens from OAuth2ClientCredentialsConfig.
	FakeToken string
	// OAuth2ClientCredentialsConfig is a TokenGenerator, such as clientcredentials.Config, that can be used as a
	// source for JWTs when accessing the rules service.
	OAuth2ClientCredentialsConfig TokenGenerator
	// Load balancing setup
	RoundRobin bool
}

// FixedBearerToken implements credentials.PerRPCCredentials by providing
// always the same token. This might be useful for local development.
type FixedBearerToken string

// GetRequestMetadata implements credentials.PerRPCCredentials
func (fb FixedBearerToken) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"Authorization": "Bearer " + string(fb),
	}, nil
}

// RequireTransportSecurity implements credentials.PerRPCCredentials
func (fb FixedBearerToken) RequireTransportSecurity() bool {
	return false
}

type TokenGenerator interface {
	Token(context.Context) (*oauth2.Token, error)
}

// OpenConnection opens a connection for a gRPC client
func (g *GrpcClient) OpenConnection(ctx context.Context) (*grpc.ClientConn, error) {
	if g == nil {
		return nil, status.Error(codes.Internal, "GrpcClient was not initialized")
	}
	var opts []grpc.DialOption
	if !g.TLS {
		opts = append(opts, grpc.WithInsecure())
	} else if g.Insecure {
		opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{InsecureSkipVerify: true})))
	}
	if g.RoundRobin {
		opts = append(opts, grpc.WithBalancerName(roundrobin.Name))
	}
	return grpc.Dial(g.Conn, opts...)
}

func (g *GrpcClient) GetCallOptions(ctx context.Context) ([]grpc.CallOption, error) {
	if g == nil {
		return nil, status.Errorf(codes.Internal, "GrpcClient was not initialized")
	}
	l := log.New(os.Stdout, "[GrpcClient.GetCallOptions] ", log.LstdFlags|log.Lshortfile|log.Lmsgprefix|log.Lmicroseconds)
	var callOpts []grpc.CallOption
	if g.OAuth2ClientCredentialsConfig != nil {
		tok, err := g.OAuth2ClientCredentialsConfig.Token(ctx)
		if err != nil {
			l.Printf("Error: %v", err)
			return callOpts, status.Errorf(codes.Unauthenticated, "Unable to retrieve authorization token from identity provider to get tcc rule: %v", err)
		}
		callOpts = append(callOpts, grpc.PerRPCCredentials(oauth.NewOauthAccess(tok)))
	} else if g.FakeToken != "" {
		callOpts = append(callOpts, grpc.PerRPCCredentials(FixedBearerToken(g.FakeToken)))
	}
	return callOpts, nil
}
